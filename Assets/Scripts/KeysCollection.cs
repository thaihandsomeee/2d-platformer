using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeysCollection : MonoBehaviour
{
    private int count;
    [SerializeField] private Text keyText;
    [SerializeField] private GameObject obstacle;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Keys"))
        {
            SoundManager.Instance.PlayCollectedSound();
            Destroy(collision.gameObject);
            count++;
            if (count >= 5) keyText.color = Color.green;
            if (count == 5) obstacle.SetActive(false);
                keyText.text = count + "/5";
        }
    }
}
